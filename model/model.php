<?php

function getPosts()
{
    $bdd = database();
    $req = $bdd->query('SELECT id, title, content, DATE_FORMAT(created_at, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM posts ORDER BY created_at DESC LIMIT 0, 5');
    return $req;
}

function getPost($postId){
    $bdd = database();
    $req = $bdd->prepare('SELECT id, title, content, DATE_FORMAT(created_at, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date_fr FROM posts WHERE id = ?');
    $req->execute(array($postId));
    $post = $req->fetch();
    // var_dump($post);
    return $post;
}


function getComments($postCommentaire){
   
    $bdd = database();
    $comments = $bdd->prepare('SELECT id, author, comment, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh%imin%ss\') AS comment_date_fr FROM comments WHERE post_id = ? ORDER BY comment_date DESC');
    $comments->execute(array($postCommentaire));

    return $comments;
}

function database(){
    
        $bdd = new PDO('mysql:host=localhost;dbname=blog_mvc;charset=utf8', 'root', '');
        $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $bdd;
    
}

/**
 * Ecrit un commentaire en base de données
 */
function postComment($postId, $author, $comment)
{
    $bdd = database();
    $comments = $bdd->prepare('INSERT into comments(post_id, author,comment, comment_date) VALUES (?,?,?, NOW()) ');
    $affectedLines = $comments->execute(array($postId, $author, $comment));

    return $affectedLines;
}