 ces 3 fichiers forment la base d'une structure MVC (Modèle - Vue - Contrôleur) :

Le modèle traite les données (modele.php)

La vue affiche les informations (affichageAccueil.php)

Le contrôleur fait le lien entre les deux (index.php)


Un pour le traitement PHP : il récupère les données de la base. On l'appelle le modèle.

Un pour l'affichage : il affiche les informations dans une page HTML. On l'appelle la vue.

Un pour faire le lien entre les deux : on l'appelle le contrôleur.


 ## Tables dans la base de données : 

id (integer) auto-incremente
title (varchar) 255 
content (text)
creation_date (datetime)



## Definition :

ob_start — Enclenche la temporisation de sortie
ob_get_clean — Lit le contenu courant du tampon de sortie puis l'efface


## l'organisation des dossiers :

- controller/ : le dossier qui contient nos contrôleurs.
- view/ : nos vues.
- model/ : notre modèle.
- public/ : tous nos fichiers statiques publics. On pourra y mettre à l'intérieur un dossier css/, images/, js/, etc.

## [Nouvelles fonctionnalité] => Ajouter des commentaires

maintenant que j'ai réorganisé mon code, on aimerai permettre aux lecteurs d'ajouter des commentaires sur les billets.

1) Ecrire un modèle(création des tables en base)
2) Ecrire le contrôleur, pour récuperer les informations et les envoyer à la vue. 
3) Ecrire la vue, pour afficher les informations
4) Mettre a jour le routeur, pour envoyer vers le bon contrôleur