<?php $title = 'mon blog'; 
ob_start();
?>

        <h1>Mon super blog !</h1>
        <p>Derniers billets du blog :</p>
 
        
        <?php
        while ($donnees = $posts->fetch())
        {
        ?>
        <div class="news">
            <h3>
                <?=  htmlspecialchars($donnees['title']); ?>
                <em>le <?=  $donnees['date_creation_fr']; ?></em>
            </h3>
            
            <p>
            <?= nl2br(htmlspecialchars($donnees['content'])); ?>
            <br />
            <em><a href="post.php?id=<?= $donnees['id']; ?>">Commentaires</a></em>
            </p>
        </div>
        <?php
        // var_dump($donnees);
        }
        $posts->closeCursor();
        ?>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>